package com.david.loginscreen;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class Home extends Activity {
Button SEND;
String  Name,Phone,Gender,Year,Course,Password;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
		Intent string=getIntent();
		Phone=string.getStringExtra("PhoneNumber");
		TextView txt=(TextView)findViewById(R.id.lblPhone);
		txt.setText(Phone);
		Password=string.getStringExtra("PassWord");
		TextView txt5=(TextView)findViewById(R.id.lblpas);
		txt5.setText(Password);
		
		Name=string.getStringExtra("Name");
		TextView txt4=(TextView)findViewById(R.id.lblName);
		txt4.setText(Name);
		Gender=string.getStringExtra("Gender");
		TextView txt1=(TextView)findViewById(R.id.lblgender);
		txt1.setText(Gender);
		
		Year=string.getStringExtra("Year");
		TextView txt2=(TextView)findViewById(R.id.textView7);
		txt2.setText(Year);
		
		Course=string.getStringExtra("Course");
		TextView txt3=(TextView)findViewById(R.id.lblcourse);
		txt3.setText(Course);
		
		SEND=(Button)findViewById(R.id.SEND);
		
		SEND.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent nextIntent=new Intent(getApplicationContext(),Register.class);
				startActivity(nextIntent);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.home, menu);
		return true;
	}

}
