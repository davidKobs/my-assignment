package com.david.loginscreen;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

public class Register extends Activity {
Button SUBMIT;
RadioButton Male, Female;
EditText txtNAME,txtCourse,txtYear;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);
		txtNAME=(EditText)findViewById(R.id.SEND);
		txtCourse=(EditText)findViewById(R.id.editText3);
		txtYear=(EditText)findViewById(R.id.editText2);
		
		Male=(RadioButton)findViewById(R.id.radioButton1);
		Female=(RadioButton)findViewById(R.id.radioButton2);
		SUBMIT=(Button)findViewById(R.id.SUBMIT);
		SUBMIT.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent nextIntent=new Intent(getApplicationContext(),Home.class);
				nextIntent.putExtra("Name",txtNAME.getText().toString());
				nextIntent.putExtra("Course",txtCourse.getText().toString());
				nextIntent.putExtra("Year",txtYear.getText().toString());
				startActivity(nextIntent);
				
			}
		});
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.register, menu);
		return true;
	}

}
