package com.david.loginscreen;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class Login extends Activity {
Button SUBMIT;
EditText Number,Password;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		Number=(EditText)findViewById(R.id.editText1);
		Password=(EditText)findViewById(R.id.editText2);
		SUBMIT=(Button)findViewById(R.id.SUBMIT);
		
		SUBMIT.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent nextIntent=new Intent(getApplicationContext(),Home.class);
				nextIntent.putExtra("PhoneNumber",Number.getText().toString());
				nextIntent.putExtra("PassWord",Password.getText().toString());
				startActivity(nextIntent);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}

}
